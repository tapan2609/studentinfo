package com.demo.student.studentInfo.utility;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ResponseUtility {

    //TODO: RESPONSE JSON FORMAT SHOULD BE {"status":"Success",data:{ } }
    public Map<?, ?> getResponseJson(int status, Map<String, Object> respJson) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", respJson);
        return responseBuilder;
    }

    public Map<?, ?> getResponseJson(int status, String respJson) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", respJson);
        return responseBuilder;
    }


    public Map<String, Object> getResponseJson(int status, Set respJson) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", respJson);
        return responseBuilder;
    }

    public Map<String, Object> getResponseJson(int status, Object a) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", a);
        return responseBuilder;
    }

//    public String encodeId(Long id){
//        String uuid = UUID.randomUUID().toString();
//        return uuid.substring(0, 4) + id + uuid.substring(4, 7);
//    }
//
//    public String decodeId(String id){
//        return id.substring(4, id.length() - 3);
//    }
}

