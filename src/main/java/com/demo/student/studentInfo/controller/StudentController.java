package com.demo.student.studentInfo.controller;

import com.demo.student.studentInfo.utility.ResponseUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/demo")
public class StudentController {

    @Autowired
    private ResponseUtility responseJsonUtil;


    @RequestMapping(value = "students/{section}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listAllStudents(@PathVariable("section") long section) {
        Map responseMap = new HashMap();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<Object>(responseMap, HttpStatus.OK);
    }
}
