package com.demo.student.studentInfo.model.dto;

import lombok.Data;

import java.util.Date;

@Data
public class StudentInfoDto {

    private int id;
    private String name;
    private int section;
    private boolean active;
    private Date admissionYear;
}
