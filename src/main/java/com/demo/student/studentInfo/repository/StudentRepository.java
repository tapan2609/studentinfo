package com.demo.student.studentInfo.repository;

import com.demo.student.studentInfo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    List<Student> findAll();

    @Query(value = "select * from student_info where section =:section",nativeQuery = true)
    Student findBySection(@Param("section") int section);

    @Query(value = "select * from student_info where section =:section and active=true",nativeQuery = true)
    Student findBySectionAndActive(@Param("section") int[] section);

    @Query(value = "select * from student_info where section =:section and admissionyear=:admissionyear and active=true",nativeQuery = true)
    Student findBySectionAndAdmissionYear(@Param("section") int[] section, Date year);




}
